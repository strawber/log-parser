<?php

class Parser {
    /**
     * Файловый указатель
     * @var false|resource
     */
    private $resource;

    /**
     * Список уникальных Url
     * @var array
     */
    private array $knownUrls = [];

    /**
     * Список методов для подсчета трафика
     * @var string[]
     */
    private array $excludeHttpCodesForCountTraffic = [
        301,
    ];

    /**
     * Количество просмотров
     * @var int
     */
    private int $countViews = 0;

    /**
     * Количество уникальных Url
     * @var int
     */
    private int $countUrls = 0;

    /**
     * Объем трафика
     * @var int
     */
    private int $countTraffic = 0;

    /**
     * Список поисковых ботов
     * @var array|int[]
     */
    private array $crawlers = [
        'Google' => 0,
        'Bing' => 0,
        'Baidu' => 0,
        'Yandex' => 0
    ];

    /**
     * Коды ответов
     * @var array|int[]
     */
    private array $statusCodes = [];

    /**
     * @throws Exception
     */
    public function __construct(string $fileName) {
        $this->resource = fopen($fileName, 'r');
        if (!is_resource($this->resource)) {
            throw new \Exception("Не удалось открыть файл {$fileName}");
        }
        $this->readFile();
    }

    /**
     * Получение результата
     * @return array
     */
    public function getResult(): array {
        return [
            'views' => $this->countViews,
            'urls' => $this->countUrls,
            'traffic' => $this->countTraffic,
            'crawlers' => $this->crawlers,
            'statusCodes' => $this->statusCodes
        ];
    }

    /**
     * Построчное чтение файла
     */
    private function readFile() {
        while (($row = fgets($this->resource)) !== false) {
            $this->parseRow($row);
        }
        fclose($this->resource);
    }

    /**
     * Анализ строки
     * @param string $row
     */
    private function parseRow(string $row) {
        $data = explode('"', $row);
        if (is_array($data)) {
            list($httpCode, $contentLength) = explode(' ', trim($data[2]));
            $url = explode(' ', $data[1])[1];
            $userAgent = trim($data[5]);

            $this->addResult($url, $httpCode, $contentLength, $userAgent);
        }
    }

    /**
     * Добавление результов
     * @param string $url
     * @param int $httpCode
     * @param int $contentLength
     * @param string $userAgent
     */
    private function addResult(string $url, int $httpCode, int $contentLength, string $userAgent) {
        $this->countViews++;

        if (!in_array($httpCode, $this->excludeHttpCodesForCountTraffic)) {
            $this->countTraffic += $contentLength;
        }

        if (!array_key_exists($url, $this->knownUrls)) {
            $this->countUrls++;
            $this->knownUrls[$url] = true;
        }

        if (!array_key_exists($httpCode, $this->statusCodes)) {
            $this->statusCodes[$httpCode] = 0;
        }
        $this->statusCodes[$httpCode]++;

        foreach ($this->crawlers as $crawler => $count) {
            $isCrawler = is_numeric(strpos($userAgent, $crawler));
            if ($isCrawler) {
                $this->crawlers[$crawler]++;
                break;
            }
        }
    }
}

if (count($argv) > 1) {
    $fileName = $argv[1];
    try {
        $parser = new Parser($fileName);
        $parserResult = $parser->getResult();
        $output = json_encode($parserResult, JSON_PRETTY_PRINT);
        $output = str_replace('"', '', $output);
        echo $output;
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}
echo PHP_EOL;
